---
name: "Thomas Lake"
# Delete the following if unused
twitter: "@DrTomLake"
github: "tswsl1989"
website: "https://sucs.org/~tswsl1989/"
---

Marine Energy researcher in the [ZCCE Energy and Environment Research Group](https://www.swansea.ac.uk/engineering/zcce/energy-environment/). One of the SUCS admin team, trying to be as useful as possible when time permits.
