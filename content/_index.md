---
title: ""
date: 2020-09-22T15:22:12+01:00
comments: false
author: Caleb Connolly
---

This blog is designed to be a flat structured site where anyone can write articles on pretty much anything, with a focus on tech and FOSS.
You can find the theme used [here](https://projects.sucs.org/kalube/sucs-hugo-theme) and the source for the site [here](https://projects.sucs.org/kalube/sucs-blog).

Please check out the [new authors guide](./posts/new-authors) for instructions on posting to the blog!