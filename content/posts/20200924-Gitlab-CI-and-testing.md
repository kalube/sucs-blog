---
title: "Attempting to deploy Hugo with GitLab CI/CD"
date: 2020-09-24T20:21:00+01:00
draft: false
toc: false
authors:
  - Thomas Lake
tags:
  - sucs
  - gitlab
---

How do you arrange to deploy a new project on to an old server, without opening up a raft of vulnerabilities?

The answer to that is, unsurprisingly, "with difficulty"
<!--more-->

SUCS has a single main server, called silver, that provides pretty much all of our public facing services. There are a few things, such as gitlab, that live as virtual machines on a second server (called iridium).

The current deployment method uses a PHP script hosted on the blogs site that, when called from an authorised IP, will download the latest blog build from GitLab and extract it on the server for the world to see.

This appeared to be a more tightly constrained solution than allowing SSH access for the user that the site runs as, but still feels like a bit of a bodge. At least it fits in with the rest of the bodges, patches and decade old "temporary" solutions that make up most of the SUCS infrastructure!
