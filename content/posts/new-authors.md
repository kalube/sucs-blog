---
title: "Contributing to the blog"
date: 2020-09-22T15:26:45+01:00
comments: false
toc: true
authors:
    - Caleb Connolly
---

How to become an author and contribute your articles
<!--more-->

## Dependencies

You will need git and hugo to contribute, install git using your distributions package manager, windows users go [here](https://git-scm.com/download/win)
```sh
# Ubuntu
sudo apt install git
# Arch
sudo pacman -S git
```
For hugo, check out https://gohugo.io/getting-started/installing/#debian-and-ubuntu

## Getting the source
Clone the source for the blog
```sh
# The theme is installed as a submodule hence recurse-submodules is needed
git clone https://projects.sucs.org/sucs/sucs-blog --recurse-submodules
```

## Add your bio
Create your author bio.
```sh
# Replace firstname/lastname with your name
hugo new authors/firstname-lastname/_index.md
```
Alternatively you can copy an existing author folder and edit to suite.

Edit content/authors/firstname-lastname/_index.md, write a short bio and fill in your handles

## Getting your change submitted

Checkout a new branch
```sh
# name-of-branch should be changed to represent the 'feature' you're adding, e.g "add-author"
git checkout -b name-of-branch
```
Commit your changes (**NOTE:** If you're a member you will need to )
```sh
# git status will show you which files changed, make sure you didn't modify any others!
git status
git add . # Will 'stage' your changed to be commited
git commit -m "new-author: Add Bob Bobson as an author"
```

### SUCS members

Create a fork of the blog by going to https://projects.sucs.org/sucs/sucs-blog and clicking the `fork` button on the top right.
```sh
# change YOUR_USER to be your sucs username
git remote add me https://projects.sucs.org/YOUR_USER/sucs-blog
git push --set-upstream me name-of-branch
```
This tells git to push your new branch up to a new repo under your user in gitlab, you can now visit https://projects.sucs.org/YOUR_USER/sucs-blog and use the banner at the top to create a merge request.

### Non-members

As only SUCS members have access to our gitlab, non members will need to email in a patch that we can apply for you.
```sh
# Generate a patch, outputted to the patches folder
git format-patch HEAD~1 -o patches
```
Send en email to staff@sucs.org with the patch as an attachment, someone will review it and apply it for you.

Alternatively you can also use `git send-email`, see this guide for a how-to: https://www.freedesktop.org/wiki/Software/PulseAudio/HowToUseGitSendEmail/