---
title: "Calebs First Post"
date: 2020-09-22T08:48:50+01:00
draft: false
toc: false
authors:
  - Caleb Connolly
tags:
  - meta
  - sucs
---

Welcome to my first post :D
<!--more-->

We developed the new SUCS blog to be an example of what we want SUCS to be, by building infrastructure that can be beneficial to everyone including non-society members. The blog serves as an example of how to implement [our styleguide](https://projects.sucs.org/sucs/sucs/-/blob/master/styleguide/styleguide.md), as well as providing a relatively simple hugo theme that can be used for other static sites by SUCS or anyone else.

The blog itself also has several uses:

It is a platform that allows us to document our journey trying to adding value to SUCS as a society in a way that benefits our members, students and the wider world. We (the exec team) want to try and build a support community where people can get help with technology/programming and potentially meet new people, we mostly hang out on Discord (where you should join us)[https://chat.sucs.org].

It allows for anyone to create articles, guides or cheat sheets on pretty much anything and have an audience of like-minded people to share and discuss with.

In the future we will add support for symposium style discourse, ie the ability to reply to an article, we can have bi-monthly topics that anyone can write an opinion piece on, and others can reply, encouraging more in-depth, well though out discussion.