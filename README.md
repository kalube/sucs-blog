# A SUCS Blog

This blog is designed to be a flat structured site where anyone can write articles on pretty much anything, with a focus on tech and FOSS.
You can find the theme used [here](https://projects.sucs.org/kalube/sucs-hugo-theme)

## How does this work?

The site is written in Hugo, a static site generator, it's designed to be served without needing a webserver/backend

## Status
The blog is live @ https://blogs.sucs.org

You can check out the latest testing version of the blog at https://sucs.org/~kalube/blogtest/


## Feature list

- [X] View posts and tags
- [X] View authors / have a bio for authors
- [X] Show latest posts, most popular tags and about section on home page
- [ ] Symposiums - show posts in reply to other posts, current theme etc